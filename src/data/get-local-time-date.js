import { TWELVE, TWENTY_FOUR } from "../common/constants.js";

let timeoutLocalTimeDate

export const getLocalTimeDate = () => {
  const localTimeDate = () => {
    const formatTime = JSON.parse(localStorage.getItem('timeFormat'))
    let timeAndDate;

    const localClock = document.querySelector('#localClock')
    const localDate = document.querySelector('#localDate')

    const options = {
      weekday: "short",
      year: "numeric",
      month: "short",
      day: "numeric",
    }

    if (formatTime === TWELVE) {
      timeAndDate = () => {
        const currentDate = new Date().toLocaleDateString("en-US", options)
        const usTime = new Date().toLocaleTimeString("en-US", { hour: '2-digit', minute: '2-digit' })
        const [time, string] = usTime.split(' ')

        const [currentHour, currentMinutes] = time.split(':')

        if (localClock) {
          localClock.innerHTML = `${currentHour} : ${currentMinutes} <div class="local-clock-format-twelve">${string}</div>`
          localDate.innerHTML = currentDate
        }
      }
    }

    if (formatTime === TWENTY_FOUR) {
      timeAndDate = () => {
        const currentDate = new Date().toLocaleDateString("en-GB", options)
        const [currentHour, currentMinutes] = new Date().toLocaleTimeString("en-GB").split(':')

        if (localClock) {
          localClock.innerHTML = `${currentHour} : ${currentMinutes}`
          localDate.innerHTML = currentDate
        }
      }
    }

    timeAndDate()
    timeoutLocalTimeDate = setTimeout(localTimeDate, 1000)
  }

  localTimeDate()
}

export const stopLocalTimeDate = () => {
  clearTimeout(timeoutLocalTimeDate)
}