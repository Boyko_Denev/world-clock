import momentTimezone from 'https://cdn.skypack.dev/moment-timezone';

// console.log(momentTimezone.tz);

// export const timeZoneList = momentTimezone.tz.names();
export const timeZoneList = momentTimezone.tz.names();