import { listSelectedLocationsView } from "../views/list-selected-locations-view.js"

export const getWorld = () => {
  const listSelectedLocations = JSON.parse(localStorage.getItem('SelectedLocations'))
  const list = document.querySelector('#list')
  list.innerHTML = '';

  !listSelectedLocations || !listSelectedLocations.length ?
    list.innerHTML = '<div class="noClockYet">No clocks yet.</div>' :
    list.innerHTML = listSelectedLocationsView(listSelectedLocations)
}