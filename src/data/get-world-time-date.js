import { TWELVE, TWENTY_FOUR } from "../common/constants.js"

let timeoutWorldTimeDate

export const getWorldTimeDate = () => {
  const worldTimeDate = () => {
    const listSelectedLocations = JSON.parse(localStorage.getItem('SelectedLocations'))
    const formatTime = JSON.parse(localStorage.getItem('timeFormat'))
    let innerFn

    if (!listSelectedLocations) {
      return;
    }

    const options = {
      weekday: "short",
      year: "numeric",
      month: "short",
      day: "numeric",
    };

    listSelectedLocations.map(el => {
      const idWorldClock = 'worldClock ' + el
      const idWorldDate = 'worldDate ' + el
      const worldClock = document.getElementById(idWorldClock)
      const worldDate = document.getElementById(idWorldDate)

      if (formatTime === TWELVE) {
        innerFn = () => {
          const currentDate = new Date().toLocaleDateString("en-US", { ...options, timeZone: el })
          const usTime = new Date().toLocaleTimeString("en-US", { timeZone: el, hour: '2-digit', minute: '2-digit' })
          const [time, string] = usTime.split(' ')

          const [currentHour, currentMinutes] = time.split(':')

          if (worldClock) {
            worldClock.innerHTML = `${currentHour} : ${currentMinutes} <div class="world-clock-format-twelve">${string}</div>`
            worldDate.innerHTML = currentDate
          }
        }
      }

      if (formatTime === TWENTY_FOUR) {
        innerFn = () => {
          const currentDate = new Date().toLocaleDateString("en-GB", { ...options, timeZone: el })
          const [currentHour, currentMinutes] = new Date().toLocaleTimeString("en-GB", { timeZone: el }).split(':')

          if (worldClock) {
            worldClock.innerHTML = `${currentHour} : ${currentMinutes}`
            worldDate.innerHTML = currentDate
          }
        }
      }

      innerFn()
    })
    timeoutWorldTimeDate = setTimeout(worldTimeDate, 1000)
  }

  worldTimeDate()
}

export const stopWorldTimeDate = () => {
  clearTimeout(timeoutWorldTimeDate)
}