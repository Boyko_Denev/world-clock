import { URL_LOCATION } from "../common/constants.js";

export const getMyLocation = () => {
  const Get = (myUrl) => {
    const HttpReq = new XMLHttpRequest();
    HttpReq.open("GET", myUrl, false);
    HttpReq.send(null);
    return HttpReq.responseText;
  }

  const result = JSON.parse(Get(URL_LOCATION));
  const myLocation = `${result.city}, ${result.countryName}`
  return myLocation
}