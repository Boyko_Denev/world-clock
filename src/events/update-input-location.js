export const renderUpdateNewLocationInput = (event) => {
  const listSelectedLocations = JSON.parse(localStorage.getItem('SelectedLocations')) || []
  const searchInput = document.querySelector('#input')
  const submitNewLocation = document.querySelector('#submit-add-location')

  submitNewLocation.disabled = true

  const inputValue = event.target.innerText
  const newLocation = inputValue.split(', ').reverse().join('/')
  searchInput.value = inputValue

  if (listSelectedLocations.includes(newLocation)) {
    submitNewLocation.disabled = true
    setTimeout(showMessage, 0)
    searchInput.value = ''
  } else {
    submitNewLocation.disabled = false
  }
}

const showMessage = () => {
  swal({
    title: 'This location is already selected'
  })
}