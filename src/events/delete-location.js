import { getWorld } from "../data/get-world.js"
import { getWorldTimeDate, stopWorldTimeDate } from "../data/get-world-time-date.js"
import { getLocalTimeDate, stopLocalTimeDate } from "../data/get-local-time-date.js"

export const deleteLocation = (locationForDelete) => {
  swal({
    title: "Are you sure you want to delete this location?",
    // icon: "warning",
    buttons: {
      confirm: "Delete",
      cancel: true
    },
    dangerMode: true,
  })
    .then((willDelete) => {
      if (willDelete) {
        deleteLocationFormList();
      }
    });

  const deleteLocationFormList = () => {
    stopLocalTimeDate()
    stopWorldTimeDate()

    const listSelectedLocations = JSON.parse(localStorage.getItem('SelectedLocations')) || []

    const tempList = listSelectedLocations.filter(value => value !== locationForDelete);
    localStorage.setItem('SelectedLocations', JSON.stringify(tempList));

    getLocalTimeDate()
    getWorld()
    getWorldTimeDate()
  }
}