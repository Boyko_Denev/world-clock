import { loadView } from "./navigation.js"

export const renderAddNewLocation = (timeZoneList) => {
  const listSelectedRegions = JSON.parse(localStorage.getItem('SelectedLocations')) || []

  const inputValue = document.querySelector('#input').value

  const newLocation = inputValue.split(', ').reverse().join('/')

  listSelectedRegions.push(newLocation)
  localStorage.setItem('SelectedLocations', JSON.stringify(listSelectedRegions))

  loadView('ClockView', timeZoneList)
}