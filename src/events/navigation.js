import { CONTAINER, TWELVE } from "../common/constants.js"
import { getLocalTimeDate, stopLocalTimeDate } from "../data/get-local-time-date.js"
import { getWorldTimeDate, stopWorldTimeDate } from "../data/get-world-time-date.js"
import { getWorld } from "../data/get-world.js"
import { addNewLocation } from "../views/add-new-location-view.js"
import { clockView } from "../views/clock-view.js"
import { formatTimeCss } from "./helper.js"

export const loadView = (view = '', timeZoneList) => {
  if (view === 'ClockView') return renderClockView()

  if (view === 'AddNewLocationView') return renderAddLocation(timeZoneList)
}

const renderClockView = () => {
  CONTAINER.innerHTML = clockView()
  let formatTime = JSON.parse(localStorage.getItem('timeFormat'))

  if (!formatTime) {
    formatTime = TWELVE
    localStorage.setItem('timeFormat', JSON.stringify(formatTime))
  }

  getLocalTimeDate()
  getWorld()
  getWorldTimeDate()
  formatTimeCss(formatTime)
}

const renderAddLocation = (timeZoneList) => {
  if (!timeZoneList) {
    swal({
      title: 'No connection with list of time zones. Try again later.'
    })

    return
  }

  stopLocalTimeDate()
  stopWorldTimeDate()

  CONTAINER.innerHTML = addNewLocation(timeZoneList)
}