export const renderInputLocation = (event, timeZoneList) => {
  const submitNewLocation = document.querySelector('#submit-add-location')
  const searchLocation = event.target.value.toLowerCase()

  submitNewLocation.disabled = timeZoneList.map(zone => zone.split('/').reverse().join(', ').toLowerCase()).includes(searchLocation) ? false : true

  const tempList = timeZoneList.filter(zone => {

    return zone.split('/').reverse().join(', ').toLowerCase().includes(searchLocation)
  }).map(el => `<li class="items">${el.split('/').reverse().join(', ')}</li>`).join('')

  document.querySelector('.options').innerHTML = tempList ? tempList : '<div>No Results</div>'
}