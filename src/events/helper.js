import { TWELVE, TWENTY_FOUR } from "../common/constants.js"

export const formatTimeCss = (formatTime) => {
  if (formatTime === TWELVE) {
    document.querySelector('.timeFormatFace-front').innerHTML = '12-h'
    document.querySelector('.timeFormatFace-back').innerHTML = '24-h'
  }

  if (formatTime === TWENTY_FOUR) {
    document.querySelector('.timeFormatFace-front').innerHTML = '24-h'
    document.querySelector('.timeFormatFace-back').innerHTML = '12-h'
  }
}