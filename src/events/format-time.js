import { getLocalTimeDate, stopLocalTimeDate } from "../data/get-local-time-date.js"
import { getWorld } from "../data/get-world.js"
import { getWorldTimeDate, stopWorldTimeDate } from "../data/get-world-time-date.js"
import { TWELVE, TWENTY_FOUR } from "../common/constants.js"

export const renderTimeFormat = () => {
  stopLocalTimeDate()
  stopWorldTimeDate()

  const timeFormat = document.querySelector('.timeFormatInner')

  timeFormat.classList.toggle('is-flipped')

  let formatTime = JSON.parse(localStorage.getItem('timeFormat'))

  formatTime = formatTime === TWELVE ? TWENTY_FOUR : TWELVE

  localStorage.setItem('timeFormat', JSON.stringify(formatTime))

  setTimeout(() => {
    getLocalTimeDate()
    getWorld()
    getWorldTimeDate()
  }, 300)
}