import { getMyLocation } from "../data/get-my-location.js"

export const clockView = () =>
  `
<div id="first-container">
  <div id="location-time-format">
    <div id="location">My Location</div>
    <div id="time-format">
      <div class="timeFormat">
        <div class="timeFormatInner">
          <div class="timeFormatFace timeFormatFace-front"></div>
          <div class="timeFormatFace timeFormatFace-back"></div>
        </div>
      </div>
    </div>
  </div>
  <div id="localClock" class="time"></div>
  <div id="localDate"></div> 
  <div id="myLocation">${getMyLocation()}</div>
</div>
<div id="second-container">
  <div>Other Locations</div>
  <div id="listContainer">
    <ul id="list"></ul>
  </div>
  <button id="add-location">Add New Location</button>
</div>
`