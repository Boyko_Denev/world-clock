export const listSelectedLocationsView = (locations) => {
  if (!locations || !locations.length) {
    return `<div class="noClockYet">No clocks yet.</div>`
  }
  return `${locations.map(simpleLocation).join('\n')}`
}

const simpleLocation = (location) =>
  `
    <li id=${location} class="list-item">
    <hr class="line1">
    <div id="selected-location">${location.split('/').reverse().join(', ').replaceAll('_', ' ')}</div>
    <div id="time-date-delBtn">
    <div id="time-date">
    <div id="worldClock ${location}" class="time time-world"></div>
    <div id="worldDate ${location}" class="worldDate"></div>
    </div>
    <span id="delBtn" class="fa-regular fa-square-minus"></span>
    </div>
    </li>
    `