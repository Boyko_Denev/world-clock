export const addNewLocation = (timeZoneList) =>
  `
  <div id="input-container">
  <input id="input" type="text" placeholder="select location"></input>
  <button id="submit-add-location" disabled={${document.querySelector('.input') ? false : true}}><i class="fa fa-check"></i></button>
  <button id="cancel-add-location"><i class="fa fa-xmark"></i></button>
  </div>
  <div id="options-container">
  <ul class="options">
  ${timeZoneList.map(el => {
    return `<li class="items">${el.split('/').reverse().join(', ')}</li>`
  }).join('\n')}
  </ul>
  </div>
  `