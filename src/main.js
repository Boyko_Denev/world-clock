import { timeZoneList } from "./data/time-zone-list.js"
import { renderTimeFormat } from "./events/format-time.js"
import { loadView } from "./events/navigation.js"
import { renderInputLocation } from "./events/render-input-location.js"
import { renderUpdateNewLocationInput } from "./events/update-input-location.js"
import { renderAddNewLocation } from "./events/render-add-new-location.js"
import { deleteLocation } from "./events/delete-location.js"

const title = document.querySelector('#title').innerHTML
document.querySelector('#titleShown').innerHTML = title

document.addEventListener('DOMContentLoaded', () => {
  document.addEventListener('click', event => {
    if (event.target.id === 'add-location') loadView('AddNewLocationView', timeZoneList)

    if (event.target.id === 'submit-add-location') renderAddNewLocation(timeZoneList)

    if (event.target.id === 'cancel-add-location') loadView('ClockView', timeZoneList)

    if (event.target.classList.contains('timeFormatInner')) renderTimeFormat()

    if (event.target.className === 'items') renderUpdateNewLocationInput(event)

    if (event.target.tagName === 'SPAN') deleteLocation(event.target.parentElement.parentElement.id)

    if (event.target.id === 'closeButtonOverlay') closeApp()

    if (event.target.id === 'minimizeButtonOverlay') minimizeApp()
  })

  let timeoutInputNewLocation = null
  document.addEventListener('keyup', (event) => {
    clearTimeout(timeoutInputNewLocation)
    timeoutInputNewLocation = setTimeout(() => {
      if (event.target.id === 'input') renderInputLocation(event, timeZoneList)
    }, 250)
  })

  loadView('ClockView', timeZoneList)
})