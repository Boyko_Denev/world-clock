const { ipcRenderer } = require('electron')
const ipc = ipcRenderer

const minimizeApp = () => {
  ipc.send('minimizeApp')
}

const closeApp = () => {
  ipc.send('closeApp')
}