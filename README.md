<img src="./icons/logo-world-clock.svg" alt="logo" width="155px" style="margin-top: 30px;">
<br>
<br>

# World Clock

<br>

## Description

"World Clock" is a single page web application that shows your location as well as the time in the given time zone. You can add and remove other cities from around the world and see the same information about them.

<br>

## Project information

- Language and version: **JavaScript ES2020**
- Platform and version: **Node 14.0+**
- Used: **Electron**, **HTML**, **CSS**

<br>

## Full project view

The app has the following views:

- **Main View** - Two sections. The first shows your location and the current time and date. The second shows other cities of your choice with the time and date in the respective time zone.
- **List View** - List of major cities worldwide to choose from and add to Main View.

<br>

## Screenshots

![Main Viw 1](images/main-view-01.jpg) ![Main Viw 2](images/main-view-02.jpg) ![List View 1](images/list-view-01.jpg) ![List View 2](images/list-view-02.jpg)

## Functionality

- **Time format** - On the Main View, you can choose in which format the time and date are displayed by switching between the _12-h_ and _24-h_ time format.
- **Delete other locations** - You can remove cities from your shortlist.
- **Add new location** - By pressing this button you will be transferred to the Second View where you can choose another city from a list to add to the your shortlist in Main View.

<br>

## Installation

Electron-builder is used for this project. So you have two options to install and use.

- Using NPM
  - Clone the repo
  ```bash
  git clone https://gitlab.com/Boyko_Denev/world-clock.git
  ```
  - Install NPM packages
  ```bash
  npm install
  ```
  - Start app
  ```
  npm start
  ```
- Install App - You can install this application easily like any other program
  - Download _dist_ folder
  - Run _World Clock Setup 1.0.0.exe_ inside it and follow instructions

<br>

## License

MIT

<br>

## Author

[@Boyko_Denev](https://www.gitlab.com/Boyko_Denev)
