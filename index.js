const { app, BrowserWindow, ipcMain } = require('electron')
const path = require('path')
const ipc = ipcMain

const isMac = process.platform === 'darwin'

const createWindow = () => {
  const mainWindow = new BrowserWindow({
    width: 350,
    minWidth: 350,
    maxWidth: 350,
    height: 730,
    minHeight: 730,
    maxHeight: 730,
    maximizable: false,
    icon: __dirname + '/icon.ico',
    frame: false,
    autoHideMenuBar: true,
    webPreferences: {
      nodeIntegration: true,
      contextIsolation: false,
      devTools: true,
      preload: path.join(__dirname, 'preload.js')
    }
  })

  mainWindow.loadFile('index.html')

  ipc.on('closeApp', () => {
    mainWindow.close()
  })

  ipc.on('minimizeApp', () => {
    mainWindow.minimize()
  })

  // Open the DevTools.
  // mainWindow.webContents.openDevTools({
  //   detach: true
  // })
}

app.whenReady().then(() => {
  createWindow()

  app.on('activate', () => {
    if (BrowserWindow.getAllWindows().length === 0) createWindow()
  })
})

app.on('window-all-closed', () => {
  if (!isMac) app.quit()
})